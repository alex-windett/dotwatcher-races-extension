import React, { useState, useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import {
  TextInput,
  DropdownListItem,
  Button,
  Paragraph,
  DropdownList,
  Dropdown,
  HelpText
} from '@contentful/forma-36-react-components';
import { init } from 'contentful-ui-extensions-sdk';
import '@contentful/forma-36-react-components/dist/styles.css';
import './index.css';
import Axios from 'axios';

export const App = ({ sdk }) => {
  const [value, setValue] = useState(sdk.field.getValue() || '');
  const [isOpen, setIsOpen] = useState(false);
  const [races, setRaces] = useState([]);
  const [loading, setLoading] = useState(true);

  const onExternalChange = value => {
    setValue(value);
  };

  const onChange = e => {
    const value = e.currentTarget.value;
    setValue(value);
    if (value) {
      sdk.field.setValue(value);
    } else {
      sdk.field.removeValue();
    }
  };

  const onClick = ({ name }) => {
    sdk.field
      .setValue(name)
      .then(data => console.log('Set value to ', data))
      .catch(err => console.log(err))
      .finally(() => setIsOpen(false));
  };

  useEffect(() => {
    sdk.window.startAutoResizer();
  }, []);

  useEffect(async () => {
    const getRaces = async () => {
      try {
        const res = await Axios({
          method: 'get',
          url: 'https://dotwatcher.cc/api/all-races'
        });

        return res.data;
      } catch (e) {
        alert(e);
      } finally {
        setLoading(false);
      }
    };

    const races = await getRaces();
    setRaces(races);
    return;
  }, []);

  useEffect(async () => {
    // Handler for external field value changes (e.g. when multiple authors are working on the same entry).
    const detatchValueChangeHandler = sdk.field.onValueChanged(onExternalChange);

    return detatchValueChangeHandler;
  }, []);

  return (
    <div style={{ 'min-height': '200px' }}>
      {loading ? (
        <Paragraph>Fetching races ... </Paragraph>
      ) : (
        <Dropdown
          isOpen={isOpen}
          onClose={() => setIsOpen(false)}
          isFullWidth={true}
          key={Date.now()} // Force Reinit
          isAutoalignmentEnabled={true}
          position="top-left"
          toggleElement={
            <Button
              size="small"
              buttonType="muted"
              indicateDropdown
              onClick={() => setIsOpen(!isOpen)}>
              Choose Race Series
            </Button>
          }>
          <DropdownList maxHeight={200}>
            {races.map((entry, index) => (
              <DropdownListItem key={`key-${index}`} onClick={() => onClick(entry)}>
                {entry.name}
              </DropdownListItem>
            ))}
          </DropdownList>
        </Dropdown>
      )}

      <div style={{ 'margin-top': '30px' }}>
        <TextInput
          width="large"
          type="text"
          id="my-field"
          testId="my-field"
          disabled
          value={value}
          onChange={onChange}
        />
      </div>

      <HelpText>Race name as found in Dotwatcher Upload. Select from the dropdown above</HelpText>
    </div>
  );
};

App.propTypes = {
  sdk: PropTypes.object.isRequired
};

init(sdk => {
  ReactDOM.render(<App sdk={sdk} />, document.getElementById('root'));
});

/**
 * By default, iframe of the extension is fully reloaded on every save of a source file.
 * If you want to use HMR (hot module reload) instead of full reload, uncomment the following lines
 */
// if (module.hot) {
//   module.hot.accept();
// }
